package com.example.nytimesapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    List<Result> movies;
    Context mContext;

    public MoviesAdapter(List<Result> movies, Context mContext) {
        this.movies = movies;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MoviesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.my_result, parent, false );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesAdapter.MyViewHolder holder, int position) {
        if (movies.get(position).getMultimedia() != null && !("null").equals(movies.get(position).getMultimedia())) {
            Picasso.get().load(movies.get(position).getMultimedia().getSrc()).fit().centerInside().into(holder.imageResult);
        } else {
            holder.imageResult.setImageResource(R.drawable.nytlogo1);
        }
        holder.titleResult.setText(movies.get(position).getDisplay_title());
        holder.authorResult.setText(movies.get(position).getByline());
        holder.headlineResult.setText(movies.get(position).getHeadline());
        holder.dateResult.setText(movies.get(position).getDate_updated());

        holder.imageResult.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, DetailActivity.class);
            if (movies.get(position).getMultimedia() != null && !("null").equals(movies.get(position).getMultimedia())) {
                intent.putExtra("image", movies.get(position).getMultimedia().getSrc());
            } else {
                intent.putExtra("image", R.drawable.nytlogo1);
            }
            intent.putExtra("title", movies.get(position).getDisplay_title());
            intent.putExtra("by", movies.get(position).getByline());
            intent.putExtra("headline", movies.get(position).getHeadline());
            intent.putExtra("date", movies.get(position).getDate_updated());
            intent.putExtra("summary", movies.get(position).getSummary_short());
            mContext.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageResult;
        TextView titleResult;
        TextView authorResult;
        TextView headlineResult;
        TextView dateResult;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageResult = itemView.findViewById(R.id.imageResult);
            titleResult = itemView.findViewById(R.id.titleResult);
            authorResult = itemView.findViewById(R.id.authorResult);
            headlineResult = itemView.findViewById(R.id.headlineResult);
            dateResult = itemView.findViewById(R.id.dateResult);
        }
    }
}
