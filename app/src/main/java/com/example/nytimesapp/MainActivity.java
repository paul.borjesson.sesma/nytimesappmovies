package com.example.nytimesapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List movies = new ArrayList<>();
    private MoviesAdapter moviesAdapter;
    private String JSON_URL = "https://api.nytimes.com/svc/movies/v2/reviews/search.json?query=";
    private EditText word;
    private TextView stLabel;
    private TextView status;
    private TextView toLabel;
    private TextView total;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        word = findViewById(R.id.word);
        btn = findViewById(R.id.button);
        stLabel = findViewById(R.id.stLabel);
        status = findViewById(R.id.status);
        toLabel = findViewById(R.id.toLabel);
        total = findViewById(R.id.total);

        recyclerView = findViewById(R.id.rView);

        btn.setOnClickListener(view -> {
            getMovies();
        });
    }

    private void getMovies() {
        JSON_URL += word.getText().toString() + "&api-key=tZoMpIOMdhqYUFANGU2S17PsGOXTDVoA";
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                response -> {
                    String status2 = response.optString("status");
                    String num_results = response.optString("num_results");
                    if (status2.equals("OK") && Integer.parseInt(num_results) > 0) {
                        JSONArray jsonArray = response.optJSONArray("results");
                        if (jsonArray != null) {
                            List<Result> results = Arrays.asList(new GsonBuilder().create().fromJson(jsonArray.toString(), Result[].class));
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            MoviesAdapter moviesAdapter= new MoviesAdapter(results, this);
                            recyclerView.setAdapter(moviesAdapter);
                        }
                    }
                    stLabel.setAlpha(1);
                    status.setAlpha(1);
                    status.setText(status2);
                    toLabel.setAlpha(1);
                    total.setAlpha(1);
                    total.setText(num_results);
                },
                error -> {Log.d("tag", "onErrorResponse: " + error.getMessage());}
        );
        requestQueue.add(jsonObjectRequest);
    }

}