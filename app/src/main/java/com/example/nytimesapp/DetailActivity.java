package com.example.nytimesapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    ImageView image;
    TextView title;
    TextView by;
    TextView headline;
    TextView date;
    TextView summary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        image = findViewById(R.id.imageDetail);
        title = findViewById(R.id.titleDetail);
        by = findViewById(R.id.byDetail);
        headline = findViewById(R.id.headlineResult);
        date = findViewById(R.id.dateDetail);
        summary = findViewById(R.id.sumDetail);

        getData();

    }

    private void getData() {
        Picasso.get().load(getIntent().getStringExtra("image"))
                .fit()
                .centerCrop()
                .into(image);
        title.setText(getIntent().getStringExtra("title"));
        by.setText(getIntent().getStringExtra("by"));
        System.out.println(getIntent().getStringExtra("headline"));
        //headline.setText(getIntent().getStringExtra("headline"));
        date.setText(getIntent().getStringExtra("date"));
        summary.setText(getIntent().getStringExtra("summary"));
    }

}